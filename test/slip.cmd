set ISO_SOURCE=D:\
set SOURCES=C:\Users\Administrator\Win2019_ISO
set BOOT_WIM=sources\boot.wim
set WIM_FILE_BASE=sources\install
set WIM_FILE=%WIM_FILE_BASE%.wim
set WIM_FILE_EXPANDED=%WIM_FILE_BASE%.swm
set WIM_FILE_CHUNK=2000
set WIM_INDEX=2
set WIMTMPDIR=C:\temp\wim
set IMAGEX_BIN="C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\DISM\imagex.exe"
set DISM_BIN="C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\DISM\dism.exe"
set VIRTIO_DRIVER_FOLDER=E:\
set OSCDIMG_BIN="C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg\oscdimg.exe"


del /s /f /q %SOURCES%\*.*
xcopy /X /S %ISO_SOURCE%*.* %SOURCES%

mkdir %WIMTMPDIR%

del /s /f /q %WIMTMPDIR%\*.*
%IMAGEX_BIN% /mountrw %SOURCES%\%BOOT_WIM% 1 %WIMTMPDIR%
%DISM_BIN% /image:%WIMTMPDIR% /add-driver /driver:%VIRTIO_DRIVER_FOLDER% /recurse
%IMAGEX_BIN% /unmount /commit %WIMTMPDIR%

del /s /f /q %WIMTMPDIR%\*.*
%IMAGEX_BIN% /mountrw %SOURCES%\%BOOT_WIM% 2 %WIMTMPDIR%
%DISM_BIN% /image:%WIMTMPDIR% /add-driver /driver:%VIRTIO_DRIVER_FOLDER% /recurse
%IMAGEX_BIN% /unmount /commit %WIMTMPDIR%

del /s /f /q %WIMTMPDIR%\*.*
%IMAGEX_BIN% /mountrw %SOURCES%\%WIM_FILE% %WIM_INDEX% %WIMTMPDIR%
%DISM_BIN% /image:%WIMTMPDIR% /add-driver /driver:%VIRTIO_DRIVER_FOLDER% /recurse
%IMAGEX_BIN% /unmount /commit %WIMTMPDIR%



%IMAGEX_BIN% /split %SOURCES%\%WIM_FILE%  %SOURCES%\%WIM_FILE_EXPANDED% %WIM_FILE_CHUNK%
del /F /Q %SOURCES%\%WIM_FILE%
%OSCDIMG_BIN% -n -m -b%SOURCES%\boot\etfsboot.com %SOURCES% C:\temp\windows.iso