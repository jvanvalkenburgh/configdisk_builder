import jinja2
import os
import tempfile

if __package__ == "":
    from local_logger import MethodLogger
else:
    from .local_logger import MethodLogger


logger = MethodLogger(__name__)


class TemplateBuilder:

    def __init__(self, context, template_text):
        # self.env = jinja2.Environment(loader=jinja2.DictLoader)
        self.context = context
        self.template_text = template_text
        self.loaders = []

        self.context_functions = {"get_foo": self.__get_foo , "fu": self.__get_foo_you}

    # @classmethod
    def __get_foo(self, message):
        return message

    def __get_foo_you(self, message):
        return str(message).upper()

    def test_values(self):
        self.context = {"metadata": {
                            "instance_id": "57b13285-96a2-49c6-a870-579794220970",
                            "launch_index": 2,
                            "server_profile": "esx",
                            "environment_name": "vsphere-primary",
                            "lp_name": "corp",
                            "availability_zone": "dc-east",
                            "domain_name": "compose.cloud"
        }}

        self.register_template(
            "dc_template", "\n".join(["""
#cloud-config
{% block time %}
ntp:
  enabled: true
  ntp_client: chrony
  servers: {{ ntp_servers if ntp_servers else "ntp1.%s.mycloud.com,  ntp2.%s.mycloud.com" % 
    (metadata.availability_zone,metadata.availability_zone) }}
{% endblock -%}
{% block hostname %}
hostname: {{ hostname if hostname else \"%s%s.%s.%s.%s\" % (metadata.server_profile, \"{:02d}\".format(metadata.launch_index), metadata.environment_name, metadata.lp_name, metadata.domain_name) }}
{% endblock %}
{% block body %}{% endblock -%}
{% block lp %}{% endblock -%}
{% block environment %}{% endblock -%}
{% block server_profile %}{% endblock -%}

"""


            ])
        )

        self.register_template(
            "lp_template", "\n".join(
                [
                    "{% extends \"dc_template\" %}",
                    "{% block body %}",
                    "{{ super() }}",
                    # "{% block lp %}"
                    "# this is lp specific commands",
                    "# ",
                    "{% endblock -%}"
                    ""

                ]))


        #self.register_template(
        #    "context", "".join([
        #     """{% set ctx = namespace() %}""",
        #    "{% set ctx.hostname=\"%s%s.%s.%s.%s\" % (metadata.server_profile, \"{:02d}\".format(metadata.launch_index), metadata.environment_name, metadata.lp_name, metadata.domain_name) %}" ,
        #     ])
        #)

        self.register_template(
            "environment_template", "\n".join(
            [
                "{% extends \"lp_template\" %}",

                "{% set hostname=\"foohost\" %}"
                "{% set ntp_servers = \"pool1.ntp.org, pool2.ntp.org\" %}"
                "{% block body %}",
                "{{ super() }}",
                "# environment specific stuff here",
                "#",
                "{% endblock %}"

                #   "{% block parent %}",
             #   "#cloud-config",
             #   "hostname: {{ hostname if hostname else ctx.hostname }}",
             #   "{% endblock %}"
            ]
        ))


        self.template_text = "\n".join([
            #"""{% set hostname=\"foohostname\" %}"""
            """{% extends "environment_template" %}""",
            """# this is the bottom level (profile) specific template""",
            "{% block body %}",
            "# setting profile",
            "{% set hostname=\"foohost_local\" %}",
            "{{ super() }}",
            #"{% set ctx.hostname=\"foohost_local\" %}",
            """{% endblock %}"""
        #            """{% block time %} """,
#"""
#ntp:
#  enabled: false
#""",
#            """{% endblock %}"""
            #"""{% block parent %}""",
            #"#cloud-config",
            #"hostname: {{ '{{ %s }}' }}-{{ '{{ %s }}' }}.{{ '{{ %s }}' }}.{{ '{{ %s }}' }}.{{ '{{ %s }}' }}" % (
            #    "metata.server_profile", """"${:02d}".format(metadata.launch_index)""",
            #    "metadata.environment_name", "metadata.lp_name", "metadata.domain_name"),
            #"hostname: {{ %s }}-{{ %s }}.{{ %s }}.{{ %s }}.{{ %s }}" % (
            #    "metadata.server_profile", """"{:02d}".format(metadata.launch_index)""",
            #    "metadata.environment_name", "metadata.lp_name", "metadata.domain_name"),
            #"""foo: {{ ":".join(["a","b","c"]) }} """,
            #"""{% set pizza = "cheese" %}""",
            #"pizza: {{ pizza }}",
            #"""{% endblock %}"""
        ])
        #  {% include "parent" %}{% block parent %}This is the {{ get_foo("bird") }} child {% endblock %}  """
        return self

    def register_template(self, template_name, template_text):
        loader = jinja2.DictLoader({template_name: template_text})
        self.loaders.append(loader)

    def register(self, function):
        pass

    def render(self):

        loaders = self.loaders
        loaders.append(jinja2.DictLoader({"foothis": self.template_text}))
        choice_loader = jinja2.ChoiceLoader(loaders)
        env = jinja2.Environment(loader=choice_loader)
        template = env.get_template("foothis")
        for function_name in self.context_functions.keys():
            template.globals[function_name] = self.context_functions[function_name]
        #template.globals["get_foo"] = self.__get_foo
        # jinja2.Template(self.template)
        text = template.render(self.context)
        return text
