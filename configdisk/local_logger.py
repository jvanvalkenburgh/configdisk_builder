import logging as pylogging
from logging import StreamHandler
import inspect
import os
import sys


class MethodLogger():

    def __init__(self, name):

        self.pylogger = pylogging.getLogger(name)
        # try to prevent dupe output
        if not self.pylogger.hasHandlers():
            formatter = pylogging.Formatter(fmt='%(asctime)s - %(levelname)s - %(message)s')
            handler: StreamHandler = pylogging.StreamHandler(sys.stdout)
            handler.setFormatter(formatter)
            self.pylogger.addHandler(handler)
            self.pylogger.setLevel(pylogging.DEBUG)
            self.pylogger.propagate = False

    @staticmethod
    def __format_message(msg):
        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 3)
        caller_name = calframe[2][3]
        caller_class = os.path.basename(calframe[2][1])
        return "%s - %s - %s" % (caller_class, caller_name, msg)

    def info(self, msg, *args, **kwargs):

        self.pylogger.info(self.__format_message(msg), *args, **kwargs)

    def warn(self, msg, *args, **kwargs):

        self.pylogger.warn(self.__format_message(msg), *args, **kwargs)

    def warning(self, msg, *args, **kwargs):

        self.logger.warning(self.__format_message(msg), *args, **kwargs)

    def error(self, msg, *args, **kwargs):

        self.pylogger.error(self.__format_message(msg), *args, **kwargs)

    def debug(self, msg, *args, **kwargs):

        self.pylogger.debug(self.__format_message(msg), *args, **kwargs)
