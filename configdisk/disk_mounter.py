#!python3
import subprocess
import os
import tempfile
from typing import Optional
import re

if __package__ == "":
    from local_logger import MethodLogger
else:
    from .local_logger import MethodLogger

import click


logger = MethodLogger(__name__)


class DiskMounter:

    def __init__(self, source_path, destination=None, loopback=False, loopback_uid=None, loopback_gid=None):
        self.source_path = source_path
        self.destination = destination
        self.mount_point = destination
        self.loopback = loopback
        self.mounted = False
        local_uid, local_gid = self.__get_uid_gid()
        self.loopback_uid = loopback_uid if loopback_uid else local_uid
        self.loopback_gid = loopback_gid if loopback_gid else local_gid
        self.dirs_to_cleanup = []
        if not self.mount_point:
            # make a temporary mount
            self.mount_point = tempfile.mkdtemp()
            self.dirs_to_cleanup.append(self.mount_point)
        else:
            if not os.path.exists(self.mount_point):
                raise Exception("If you specify a mount point, it must exist - %s" % self.mount_point)

    def __enter__(self):

        self.mount_point = self.__get_mount(0)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.mounted:
            self.__unmount()
        pass

    def mount(self):
        if self.mounted:
            raise Exception("Already mounted")
        self.mount_point = self.__get_mount(0)
        return self.mount_point

    def unmount(self):
        if not self.mounted:
            raise Exception("Already unmounted")
        self.__unmount()

    def _insert_position(self, position, list1, list2):
        return list1[:position] + list2 + list1[position:]

    def __get_uid_gid(self):
        result = subprocess.run(["id"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        id_stanzas = result.stdout.decode("utf8").strip().split("\n")[0].split(" ")
        uid = "".join(list(map(str, re.findall(r'\d+', id_stanzas[0]))))
        gid = "".join(list(map(str, re.findall(r'\d+', id_stanzas[1]))))
        return uid, gid


    def __get_mount_by_type(self):
        args = None
        if "qcow2" in self.source_path:
            logger.debug("mounting (guestmount) %s on temporary location %s" % (self.source_path, self.mount_point))
            # TODO - fix hard sda1 - will only work for vfat and need better solution for linux use case
            args = ["guestmount", "-a", self.source_path, "-m", "/dev/sda1", self.mount_point]
        else:
            logger.debug("mounting %s on temporary location %s" % (self.source_path, self.mount_point))
            args = ["sudo", "mount", self.source_path, self.mount_point]
            if self.loopback:
                args = self._insert_position(2, args,
                                             ["-o", "loop,uid=%s,gid=%s" % (self.loopback_uid, self.loopback_gid), ])
                logger.debug("Making the mount a loop")

        result = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return result

    def __get_mount(self, loop_control=0) -> Optional[str]:
        loop_control = loop_control + 1
        if loop_control > 3:
            return None

        result = self.__get_mount_by_type()
        if result.stderr:
            stderr_lines = result.stderr.decode('utf-8').strip().split('\n')
            for line in stderr_lines:
                if "The disk contains an unclean file system" in line:
                    # unmount and run ntfsfix
                    self.__unmount()
                    # prob need to address if this is FAT
                    logger.error("Unclean filesystem")
                    # cls.ntfsfix(partition)
                    return self.__get_mount(loop_control + 1)

        if result.returncode != 0:
            self.__output("get_temporary_mount", [result.stdout, result.stderr])
            logger.error("mount of %s to %s failed " % (self.source_path, self.mount_point))
            return None

        self.mounted = self.mount_point is not None

        return self.mount_point

    def __unmount(self) -> bool:

        if "qcow2" in self.source_path:
            logger.debug("unmount - unmounting %s" % self.source_path)
            result = subprocess.run(["guestunmount", self.mount_point])
        else:
            logger.debug("unmount - unmounting %s" % self.source_path)
            result = subprocess.run(["sudo", "umount", self.source_path])

        if result.returncode != 0:
            stderr = result.stderr.decode('utf-8').strip().split('\n')
            logger.error("unmount of %s failed - message %s" % (self.source_path, str(stderr)))

        for temp_dir_to_dlete in self.dirs_to_cleanup:
            logger.info("unmount - removing %s" % temp_dir_to_dlete)
            subprocess.run([
                "rm", "-rf", temp_dir_to_dlete])

        if result.returncode is 0:
            self.mounted = False
            return True
        else:
            self.mounted = True
            return False

    @classmethod
    def __output(cls, label, streams):
        if streams:
            for stream in streams:
                if stream:
                    lines = stream.decode('utf-8').strip().split('\n')
                    for line in lines:
                        logger.debug("%s - %s" % (label, line))

