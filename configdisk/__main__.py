
from .local_logger import *
from .cli import cli


import sys
import traceback

logger = MethodLogger(str(__name__).replace("_", ""))


def __stacktrace():
    lines = traceback.format_exc().strip().split("\n")
    for line in lines:
        logger.error("err - %s" % line)


if __name__ == '__main__':

    # main process
    try:
        cli(prog_name='image_builder')
        sys.exit(0)

    # unhandled exception
    except Exception as e:
        __stacktrace()
        sys.exit(255)
