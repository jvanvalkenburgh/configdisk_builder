#!/usr/bin/python3
import subprocess
import os

if __name__ == '__main__':
    from local_logger import MethodLogger
    from _version import __version__
    from floppy_builder import FloppyBuilder
    from configdrive_builder import ConfigDrive
else:
    from .local_logger import MethodLogger
    from ._version import __version__
    from .floppy_builder import FloppyBuilder
    from .configdrive_builder import ConfigDrive

import click


logger = MethodLogger(__name__)


def __expand_filename(filename):
    from pathlib import Path
    import os
    #  return Path(filename).expanduser()
    new_name = os.path.expandvars(os.path.expanduser(filename))
    if filename is not new_name:
        logger.info("expanding file path to %s" % new_name)
    return new_name

def __safe_list(item):
    try:
        return list(iter(item))
    except TypeError as te:
        return list(item)


@click.group(name="image_builder")
@click.version_option(version=__version__)
def cli():
    """Image builder for hacking configdisks
    """


@cli.command("build_floppy", short_help="Build floppy")
@click.option("--output_file", "-o", help="image to write to", multiple=False, required=True)
@click.option("--artifact", "-a", help="artifacts to copy in, in form of source:target", multiple=True, required=True)
def build_floppy(artifact, output_file):
    """
    """
    artifacts = __safe_list(artifact)
    output_file = __expand_filename(output_file)

    with FloppyBuilder(output_file, volume_name=None) as floppy:
        for artifact_entry in artifacts:
            source, destination = None, None
            if ":" in artifact_entry:
                artifact_sections = artifact_entry.split(":")
                source, destination = __expand_filename(artifact_sections[0]), __expand_filename(artifact_sections[1])
            else:
                source, destination = __expand_filename(artifact_entry), os.path.basename(artifact_entry)

            floppy.copy_in_file(source, destination)


@cli.command("build_configdrive", short_help="Build floppy")
@click.option("--output_file", "-o", help="image to write to", multiple=False, required=True)
@click.option("--artifact", "-a", help="artifacts to copy in, in form of source:target", multiple=True, required=True)
def build_configdrive(artifact, output_file):
    """
    """
    artifacts = __safe_list(artifact)
    output_file = __expand_filename(output_file)

    with ConfigDrive(output_file) as configdrive:
        pass

        """
        for artifact_entry in artifacts:
            source, destination = None, None
            if ":" in artifact_entry:
                artifact_sections = artifact_entry.split(":")
                source, destination = __expand_filename(artifact_sections[0]), __expand_filename(artifact_sections[1])
            else:
                source, destination = __expand_filename(artifact_entry), os.path.basename(artifact_entry)

            floppy.copy_in_file(source, destination)
        """

if __name__ == '__main__':
    with cli() as main_process:
        pass

