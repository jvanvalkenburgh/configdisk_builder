import subprocess
import os
import tempfile
from typing import Optional

if __package__ == "":
    from local_logger import MethodLogger
    from disk_mounter import DiskMounter
else:
    from .local_logger import MethodLogger
    from .disk_mounter import DiskMounter

import click


logger = MethodLogger(__name__)


class FloppyBuilder:

    _FLOPPY_SIZE = "10M"

    def __init__(self, output_filename, volume_name=None):

        # make sure right prog is installed
        self.__check_prerequisites()

        self.have_files = False
        self.output_filename = output_filename
        self.is_raw_device = False if ".qcow2" in output_filename else True
        self.volume_name = volume_name
        self._build_empty_image()
        self.mounter = None
        self.mount_point = None


    def __check_prerequisites(self):
        try:
            subprocess.run(["mkfs.msdos", "--help"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).check_returncode()
        except subprocess.CalledProcessError as e:
            logger.error("Exception - %s" % str(e))
            logger.error("make sure mkfs.msdos is available")
            raise e

    def __enter__(self):
        self.__mount()
        return self

    def mount(self):
        self.__mount()

    def __mount(self):
        self.mounter = DiskMounter(self.output_filename, loopback=True, destination=None)
        self.mount_point = self.mounter.mount()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.done()
        pass

    def done(self):
        if self.mounter:
            self.ls()
            self.mounter.unmount()
        pass


    def ls(self):
        if self.mounter:
            res = subprocess.run(["ls", "-al", self.mounter.mount_point], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            self.__output("ls", [res.stdout, res.stderr])
        else:
            raise Exception("not mounted")

    @classmethod
    def __output(cls, label, streams):
        if streams:
            for stream in streams:
                if stream:
                    lines = stream.decode('utf-8').strip().split('\n')
                    for line in lines:
                        logger.debug("%s - %s" % (label, line))


    def copy_in_file(self, source, relative_destination):

        # this way we don't need to make initializing this a prereq call
        if not self.mounter:
            self.__mount()

        destination = "%s/%s" % (self.mounter.mount_point, relative_destination)
        subprocess.run([
            # "sudo",
            "mkdir", "-p", os.path.dirname(destination)]).check_returncode()
        res = subprocess.run([
            # "sudo",
            "cp", source, destination])
        self.__output("copy", [res.stdout, res.stderr])
        logger.info("Copied file %s to %s" % (source, destination))
        # self.have_files = True

    def _build_empty_image(self):

        args = []
        if not self.is_raw_device:
            res = subprocess.run([
                "bash", "-c",
                " ".join(["pwd;", "qemu-img", "create", "-f", "qcow2", self.output_filename, str(self._FLOPPY_SIZE), "; ls -al"])],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
            self.__output("qcow2 create %s" % self.output_filename, [res.stdout, res.stderr])

            args = ["virt-format", "-a", self.output_filename,
                    "--filesystem=vfat", "--label=%s" % self.volume_name, "--partition=mbr"]
        else:
            subprocess.run(["rm", "-rf", self.output_filename], stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE).check_returncode()
            args = ["mkfs.msdos", "-C", self.output_filename, str(self._FLOPPY_SIZE)]
            if self.volume_name:
                args = args[:1] + ["-n", self.volume_name] + args[1:]
                logger.info("args now %s" % str(args))

        res = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.__output("create %s" % self.output_filename, [res.stdout, res.stderr])
        res.check_returncode()
        logger.info("Built empty image %s" % self.output_filename)
